<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_PRESENTS">
    <div class="modal-dialog modal-el" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
            <div class="modal-body">
                <div class="product-list mvn pvn">
                    <?php for($i=0; $i < 8; $i++):?>
                        <div class="product-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_presents_transform.php";?>
                        </div>
                    <?php endfor;?>
                </div>
            </div>
        </div>
    </div>
</div>