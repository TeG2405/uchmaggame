// Доработаны поведения модальных окон.
// Теперь открытые окна скрываются автоматически при открытии нового;
// Если вы добавили новое модальное окно на страницу при помощи ajax вызовите событе "onDocumentChange"
// $(document).trigger('onDocumentChange');
+function($){
    $.fn.modal.stack = [];
    var $modals = $('.modal');
    $(document).on('onDocumentChange', function () {
        $modals = $('.modal');
        $modals
            .not('.init')
            .addClass('init')
            .on('show.bs.modal', function (e) {
                if($modals.filter('.in').length){
                    $.fn.modal.stack.push($modals.filter('.in').modal('hide'));
                    setTimeout(function () {
                        $(e.target).modal('show');
                    }, $.fn.modal.Constructor.TRANSITION_DURATION * 1.5);
                    return false;
                }
            })
            .on('hide.bs.modal', function (e) {
                var $temp;
                if($.fn.modal.stack.length){
                    setTimeout(function () {
                        this.modal('show');
                    }.bind($.fn.modal.stack[$.fn.modal.stack.length - 1]), $.fn.modal.Constructor.TRANSITION_DURATION * 1.5);
                    $.fn.modal.stack.splice(-1, 1);
                }
            });
    }).trigger('onDocumentChange');

    $(document).on('keydown', function (e) {
        if(e.ctrlKey && e.keyCode === 13){
            var $this = $modals.filter('#MODAL_SEND_ERROR');
            try { $this.find('#ERROR_TEXT').val(window.getSelection().toString())}
            catch (e) {
                $this.find('#ERROR_TEXT').val(document.selection.createRange().htmlText);
            }
            if($this.find('#ERROR_TEXT').val()) $this.modal('show');
        }
    })
}(jQuery);
