<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_SUBSCRIBE">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Подписка на раздел</div>
            </div>
            <div class="modal-body">
                <form>
                    <p>При появлении нового товара Вам придет напоминание на почту.</p>
                    <div class="form-group">
                        <label class="control-label required" for="SUBSCRIBE_EMAIL">Email</label>
                        <input type="email" class="form-control" id="SUBSCRIBE_EMAIL" placeholder="alexanderkarpov@gmail.com">
                    </div>
                    <div class="form-group">
                        <label class="control-label required" for="SUBSCRIBE_PERIODICITY">Периодичность</label>
                        <select class="form-control" name="SUBSCRIBE_PERIODICITY" id="SUBSCRIBE_PERIODICITY">
                            <option value="_1">Каждый день</option>
                            <option value="_2">Каждый месяц</option>
                            <option value="_3">Каждый год</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="control-label">Параметры подписки</div>
                        <div><span class="text-muted">Раздел:</span><span> Преподавателям</span></div>
                    </div>
                    <button type="submit" class="btn btn-primary">Подписаться</button>
                </form>
            </div>
            <div class="modal-footer">
                Ваш список подписок доступен в <a href="#">личном кабинете</a>
            </div>
        </div>
    </div>
</div>