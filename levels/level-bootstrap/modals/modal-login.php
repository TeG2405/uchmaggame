<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_LOGIN">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="LOGIN_EMAIL">Логин или E-mail:</label>
                        <input type="email" class="form-control" id="LOGIN_EMAIL" placeholder="Логин / E-mail">
                    </div>
                    <div class="form-group">
                        <label for="LOGIN_PASSWORD">Пароль:</label>
                        <div class="control-input">
                            <input type="password" class="form-control" id="LOGIN_PASSWORD" placeholder="Пароль">
                            <button class="control-input__btn fum fum-eye" type="button" onclick="LOGIN_PASSWORD.type = LOGIN_PASSWORD.type == 'text' ? 'password' : 'text' "></button>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php include "../levels/level-blocks/social/social-circle.php";?>
                    </div>
                    <div class="form-group modal-controls mbs">
                        <div class="modal-controls__col">
                            <a class="btn btn-primary btn-lg" href="?login=Y">Вход</a>
                        </div>
                        <div class="modal-controls__col  text-right">
                            <button class="btn btn-default btn-lg" type="button"  data-dismiss="modal" aria-label="Close">Регистрация</button>
                        </div>
                    </div>
                    <a class="link-underline" href="#">Забыли свой пароль?</a>
                </form>
            </div>
        </div>
    </div>
</div>