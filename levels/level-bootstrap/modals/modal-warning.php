<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_WARNING">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-warning">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Важная информация</div>
            </div>
            <div class="modal-body">
                <p class="lead mbl"><b class="text-warning">Данный продукт - программа для установки через интернет. ЭТО НЕ ДИСК!</b></p>
                <p><b>Версия для скачивания через интернет и дисковая версия продукта имеют одинаковое содержание. Интернет-версия отличается от диска только тем, что для ее установки не нужно вставлять диск с программой в компьютер. После оплаты интернет-версии, на ваш e-mail придет ссылка, по которой ее можно будет скачать и установить.</b></p>
                <p>Обратите внимание, что если вы заказываете программу для установки через интернет вместе с другими товарами и хотите оплатить заказ при получении, то в заказе будет лежать только инструкция по установке программы.</p>
                <p>Если вы хотите максимально быстро получить программу, оплатите ее отдельно или внесите предоплату за заказ.</p>
            </div>
            <div class="modal-footer">
                <div class="modal-controls">
                    <div class="modal-controls__col">
                        <button class="btn btn-primary" type="button"  data-dismiss="modal" aria-label="Close">Продолжить</button>
                    </div>
                    <div class="modal-controls__col text-right">
                        <button class="btn btn-default" type="button"  data-dismiss="modal" aria-label="Close">Отмена</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>