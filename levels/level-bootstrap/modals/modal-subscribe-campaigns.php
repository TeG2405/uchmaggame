<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_SUBSCRIBE_CAMPAIGNS">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Подписка на акции</div>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="control-label required" for="SUBSCRIBE_CAMPAIGNS_NAME">Ваше имя</label>
                        <input type="text" class="form-control" id="SUBSCRIBE_CAMPAIGNS_NAME" placeholder="Мария Иванова">
                    </div>
                    <div class="form-group">
                        <label class="control-label required" for="SUBSCRIBE_CAMPAIGNS_EMAIL">Email</label>
                        <input type="email" class="form-control" id="SUBSCRIBE_CAMPAIGNS_EMAIL" placeholder="alexanderkarpov@gmail.com">
                    </div>
                    <button type="submit" class="btn btn-primary">Подписаться</button>
                </form>
            </div>
            <div class="modal-footer">
                Ваш список подписок доступен в <a href="#">личном кабинете</a>
            </div>
        </div>
    </div>
</div>