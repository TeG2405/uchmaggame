<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_ADD_PRODUCT_BY_CODE">
    <div class="modal-dialog modal-ms" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Добавление товара по коду</div>
            </div>
            <div class="modal-body">
                <p>Пожалуйста, укажите через запятую список кодов товаров, которые будут добавлены в корзину</p>
                <form>
                    <div class="row">
                        <div class="col-xs-12 col-sm-9">
                            <div class="form-group"> 
                                <label for="ADD_PRODUCT_BY_CODE__TEXT">Список товаров:</label>
                                <input class="form-control" id="ADD_PRODUCT_BY_CODE__TEXT" type="text">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <div class="form-group">
                                <label>Количество:</label>
                                <?php include "../levels/level-blocks/count/count-basket.php";?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><span class="phm">Добавить</span></button>
                </form>
            </div>
        </div>
    </div>
</div>