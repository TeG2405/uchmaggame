<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_SEND_ERROR">
    <div class="modal-dialog modal-ms" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Отправка ошибки</div>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="ERROR_TEXT">Текст ошибки:</label>
                        <textarea id="ERROR_TEXT" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="form-group"> 
                        <label for="COMMENT_TEXT">Текст ошибки:</label>
                        <textarea id="COMMENT_TEXT" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"><span class="phm">Отправить</span></button>
                        <button type="submit" class="btn btn-default" data-dismiss="modal" aria-label="Close"><span class="phm">Отменить</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>