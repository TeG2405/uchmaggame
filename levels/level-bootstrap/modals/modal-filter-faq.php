
<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_FILTER_FAQ">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Как пользоваться фильтром?</div>
            </div>
            <div class="modal-body">
                <p>Фильтр позволит Вам быстрее найти то, что Вы ищите.</p>
                <ul class="list-dash">
                    <li>При нажатии на такие пункты как "<u>Тип</u>", откроется окно, в котором нужно отметить интересующие Вас параметры поиска.</li>
                    <li>В поля "<u>Автор</u>", "<u>Производитель</u>" можно ввести название целиком или его часть.</li>
                    <li>Фильтр по <u>стоимости</u> можно установить ползунком.</li>
                </ul>
                <p>После настройки параметров фильтрации нажмите "<b>ПРИМЕНИТЬ</b>".</p>
                <p>Чтобы сбросить фильтр и отобразить все товары раздела нажмите на "<u>сбросить</u>"</p>
            </div>
            <div class="modal-footer">
                Ваш список подписок доступен в <a href="#">личном кабинете</a>
            </div>
        </div>
    </div>
</div>