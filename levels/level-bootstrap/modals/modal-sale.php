<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_SALE">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Нашли этот товар дешевле?</div>
            </div>
            <div class="modal-body">
                <form>
                    <p>Пришлите нам ссылку на этот товар в другом магазине, ответ поступит на указанный вами e‑mail.</p>
                    <div class="form-group">
                        <label class="control-label required" for="SALE_NAME">Ваше имя</label>
                        <input type="text" class="form-control" id="SALE_NAME" placeholder="Иван Иванович Иванов">
                    </div>
                    <div class="form-group">
                        <label class="control-label required" for="SALE_EMAIL">E-mail</label>
                        <input type="email" class="form-control" id="SALE_EMAIL" placeholder="alexanderkarpov@gmail.com">
                    </div>
                    <div class="form-group">
                        <label class="control-label required" for="SALE_PHONE">Телефон</label>
                        <input type="tel" class="form-control" id="SALE_PHONE" placeholder="+7 ___ __ __">
                    </div>
                    <div class="form-group">
                        <label class="control-label required" for="SALE_LINK">Ссылка на товар в другом магазине</label>
                        <input type="text" class="form-control" id="SALE_LINK" placeholder="https://www.uchmag.ru/estore/e164410/">
                    </div>
                    <div class="form-group">
                        <label class="control-label required" for="SALE_TEXT">Комментарий</label> 
                        <textarea class="form-control" rows="4" id="SALE_TEXT"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Отправить заявку</button>
                </form>
            </div>
        </div>
    </div>
</div>