<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_TABLE_SALE">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
            <div class="modal-body">
                <div class="alert alert-success mtl">
                    Для получения скидки 7% необходимо, чтобы сумма заказа была на 20 руб. больше.
                </div>
               <table class="table table-condensed table-striped table-bordered mbn">
                   <thead>
                   <tr>
                       <th>Заказ на сумму</th>
                       <th>Скидка</th>
                   </tr>
                   </thead>
                   <tbody>
                   <tr>
                       <td>от 350 руб.</td>
                       <td>10%</td>
                   </tr>
                   <tr>
                       <td>от 450 руб.</td>
                       <td>12%</td>
                   </tr>
                   <tr>
                       <td>от 550 руб.</td>
                       <td>13%</td>
                   </tr>
                   <tr>
                       <td>от 650 руб.</td>
                       <td>15%</td>
                   </tr>
                   <tr>
                       <td>от 750 руб.</td>
                       <td>20%</td>
                   </tr>
                   <tr>
                       <td>от 850 руб.</td>
                       <td>80%</td>
                   </tr>
                   </tbody>
               </table>
            </div>
            <div class="modal-footer">
                <div class="modal-controls">
                    <div class="modal-controls__col">
                        <button class="btn btn-primary" type="button"  data-dismiss="modal" aria-label="Close">Перейти в каталог</button>
                    </div>
                    <div class="modal-controls__col text-right">
                        <button class="btn btn-default" type="button"  data-dismiss="modal" aria-label="Close">Оформить заказ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>