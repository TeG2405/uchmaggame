<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_FAST_VIEW_PRODUCT">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
            <div class="modal-body">
                <div class="view-product">
                    <div class="view-product__labels">
                        <div class="label label-blue">скидка 10%</div>
                        <div class="label label-red">Акция</div>
                    </div>
                    <div class="view-product__inner">
                        <div class="mbm">
                            <div class="view-product__image">
                                <a class="view-product__link" href="#">
                                    <img class="img-responsive" src="images/thumbnail-product.jpg" alt="">
                                </a>
                            </div>
                            <div class="mtl">
                                <div class="mbx text-muted">Учебная и научная литература</div>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="view-product__body">
                            <a href="#" class="view-product__caption">
                                Сезонные прогулки. Карта-план для воспитателя. Подготовительная группа (от 6 до 7 лет). Март-август: Комплект из 112 тематических карт для организации прогулок с детьми на каждый день по программе "Детство"
                            </a>
                            <div class="view-product__list">
                                <table>
                                    <tr>
                                        <td>Код:</td>
                                        <td>НБ-438</td>
                                    </tr>
                                    <tr>
                                        <td>Авторы-составители:</td>
                                        <td><a class="link-underline" href="#">Небыкова О.Н.</a></td>
                                    </tr>
                                    <tr>
                                        <td>Издательство:</td>
                                        <td><a class="link-underline" href="#">Учитель</a>, 2017</td>
                                    </tr>
                                    <tr>
                                        <td>Серия:</td>
                                        <td><a class="link-underline" href="#">Карточное планирование в ДОО</a></td>
                                    </tr>
                                    <tr>
                                        <td>Страниц:</td>
                                        <td>180</td>
                                    </tr>
                                    <tr>
                                        <td>Рейтинг:</td>
                                        <td><?php include "../levels/level-blocks/rating/rating.php";?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="view-product__row">
                                <div class="view-product__price">
                                    <div class="view-product__price-label view-product__price-label_success">
                                        <i class="fum fum-check"></i>
                                        есть на складе
                                    </div>
                                    <div class="view-product__price-old"><?= rand(55, 999)?> <?= rand(555, 999)?><small>,20</small> руб.</div>
                                    <div class="view-product__price-base"><?= rand(1, 55)?> <?= rand(100, 500)?><small>,20</small> руб.</div>
                                </div>
                                <div class="view-product__control">
                                    <?php if(true):?>
                                        <a class="btn btn-lg btn-primary phl" data-toggle="modal" href="#MODAL_BASKET">В корзину</a>
                                    <?php else: $thumbnailBasketState = 0;?>
                                        <a class="btn btn-lg btn-default phl" data-toggle="modal" href="#MODAL_SUBSCRIBE">
                                        <span class="view-product__btn-inner">
                                            <i class="fum fum-envelope mrs"></i>
                                            <small>сообщить <br> о поступлении</small>
                                        </span>
                                        </a>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>