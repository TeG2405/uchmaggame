<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_PRICES">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Прайсы</div>
            </div>
            <div class="modal-body">
                <h2 class="mtn">Стандартные прайсы</h2>
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th>Прайс-листы</th>
                            <th class="text-nowrap">Последнее обновление</th>
                            <th style="width: 200px">Файл</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for($i=0; $i<3; $i++):?>
                            <tr>
                                <td>Прайс УчМага (полностью)</td>
                                <td>09.11.2017</td>
                                <td>скачать <a href="#">XLS</a>, <a href="#">YML</a></td>
                            </tr>
                        <?php endfor;?>
                        </tbody>
                    </table>
                </div>
                <h2>Оптовые прайсы</h2>
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th>Прайс-листы</th>
                            <th class="text-nowrap">Последнее обновление</th>
                            <th style="width: 200px">Файл</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for($i=0; $i<10; $i++):?>
                            <tr>
                                <td>Прайс УчМага (полностью)</td>
                                <td>09.11.2017</td>
                                <td><a href="#">скачать</a></td>
                            </tr>
                        <?php endfor;?>
                        </tbody>
                    </table>
                </div>

                <p>Своим клиентам мы можем доставить любую партию нашей книгопечатной и электронной продукции по всей территории России и в регионы ближнего зарубежья. Мы готовы осуществить отгрузку и доставку заказов транспортными компаниями, автотранспортными предприятиями, рейсовыми автобусами, почтовыми посылками.</p>
                <p>Для заключения договора обращайтесь к менеджерам по электронной почте <a href="mailto:manager@uchitel-izd.ru">manager@uchitel-izd.ru</a> (указав в качестве темы письма название Вашей организации) или по т/ф. (8442) 42-25-58, 42-39-21, 42-39-24.</p>
            </div>
        </div>
    </div>
</div>