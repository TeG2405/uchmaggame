<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_REGISTRATION">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
            <div class="modal-body">
                <form class="mtm">
                    <div class="form-group">
                        <label for="REGISTRATION_EMAIL">На какой e-mail отправить информацию о заказе?</label>
                        <input type="email" class="form-control" id="REGISTRATION_EMAIL" placeholder="Логин / E-mail">
                    </div>
                    <div class="form-group">
                        <label for="LOGIN_PASSWORD">Войти через:</label>
                        <?php include "../levels/level-blocks/social/social-circle.php";?>
                    </div>
                    <div class="form-group">
                        <div class="checkbox mvn">
                            <label>
                                <span class="input input_type_checkbox">
                                    <input checked type="checkbox">
                                    <span class="input__icon fum"></span>
                                </span>
                                Я принимаю <a href="#">пользовательское соглашение</a> интернет-магазина
                            </label>
                        </div>
                    </div>
                    <div class="form-group modal-controls mbs">
                        <div class="modal-controls__col">
                            <a class="btn btn-primary btn-lg" href="?login=Y">Продолжить</a>
                        </div>
                        <div class="modal-controls__col">
                            <a class="link-underline" href="#">Уже зарегистрированы?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>