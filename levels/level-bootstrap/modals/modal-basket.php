<div class="modal fade" tabindex="-1" role="dialog" id="MODAL_BASKET">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
                <div class="modal-title">Товар добавлен в корзину</div>
            </div>
            <div class="modal-body">
                <p>Физическое развитие детей 2-3 лет. Планирование НОД. Технологические карты. Декабрь-февраль: 32 карты</p>
                <div class="well well-warning well-ex mbn">
                    Количество товара не может быть отрицательным или нулевым. Если вы хотите удалить товар, вы можете удалить его, используя корзину.
                </div>
                <div class="well well-success well-ex mts mbl">
                    <div class="counter">
                        <div class="counter__title prs">Этого товара в корзине:</div>
                        <div class="counter__control">
                            <input class="form-control mrs" type="text" value="1">
                            шт.
                        </div>
                        <div class="mls"> 
                            <span class="link-dashed">изменить</span>
                        </div>
                    </div>
                </div>
                <div class="modal-controls">
                    <div class="modal-controls__col">
                        <button class="btn btn-primary btn-block" type="button"  data-dismiss="modal" aria-label="Close">Продолжить покупки</button>
                    </div>
                    <div class="modal-controls__col">
                        <button class="btn btn-default btn-block" type="button"  data-dismiss="modal" aria-label="Close">Оформить заказ</button>
                    </div>
                </div>
                <div class="modal-well mtl pbm">
                    <h2 class="mtn">С этим товаром покупают</h2>
                    <?php include "../levels/level-blocks/carousel-row/carousel-row_small.php";?>
                </div>
            </div>
            <div class="modal-footer">
                <div class="checkbox mvn">
                    <label>
                        <span class="input input_type_checkbox">
                            <input checked type="checkbox">
                            <span class="input__icon fum"></span>
                        </span>
                        Не показывать это окно 1 час
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>