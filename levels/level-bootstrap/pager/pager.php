<nav aria-label="Навигация по акциям">
    <ul class="pager">
        <li class="previous disabled"><a href="#"><span aria-hidden="true"> << </span> Предыдущая</a></li>
        <li class="next"><a href="#">Следующая <span aria-hidden="true"> >> </span></a></li>
    </ul>
</nav>