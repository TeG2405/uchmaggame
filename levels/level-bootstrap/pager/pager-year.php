<nav aria-label="Навигация по акциям">
    <ul class="pager">
        <li class="previous hidden-xs disabled"><a href="#"><span aria-hidden="true"> << </span> Предыдущий</a></li>
        <li class="pager__date">
            <input class="form-control" data-type="month" type="text" value="Февраль 2018" placeholder="">
        </li>
        <li class="next hidden-xs"><a href="#">Следующий <span aria-hidden="true"> >> </span></a></li>
    </ul>
</nav>