<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
    <div class="alert__inner">
        <div>
            <p>Вы запистили изменение заказа №563163. Этот заказ был отменен, чтобы вы смогли оформить новый. Товары из него были перемещены в вашу корзину.</p>
            <ul class="list-circle mbn mll">
                <li>Если в заказе были подарки, они стали обычными товарами, вы можете удалить их из корзины и выбрать подарки повторно</li>
                <li>Проверьте колличество товара перед оформлением заказа</li>
            </ul>
        </div>
    </div>
</div>