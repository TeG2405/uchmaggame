<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
    <div class="alert__inner">
        <div>
            Вы можете бесплатно добавить один из подарков. Для этого нажмите на кнопку "В подарок" и подарок добавится в корзину
        </div>
    </div>
</div>