<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fum fum-close" aria-hidden="true"></span></button>
    <div class="alert__inner">
        <i class="alert__icon fum fum-exclamation-circle mrl"></i>
        <div>
            Ваша корзина пуста. <br>
            Воспользуйтесь <a href="#">поиском</a>, чтобы найти нужную Вам продукцию, или <a href="#">каталогом товаров</a>. Ниже выведен список <a href="#">бестселлеров</a>.
        </div>
    </div>
</div>