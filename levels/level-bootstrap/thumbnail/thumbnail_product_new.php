<?php
    $LABELS = Array('blue', 'purple', 'green');
    $LABELS_VALUE = Array(
        'blue'=> 'скидка 10%',
        'purple'=> 'скидка 15%',
        'green'=> 'скидка 5%',
    );
    shuffle($LABELS);
    $DESCRIPTIONS = Array(
        'Физическое развитие детей 2-3 лет. Планирование НОД. Технологические карты. Декабрь-февраль: 32 карты',
        'НОД. Речевое развитие детей 6-7 лет. Сентябрь - декабрь: 72 карты с методическим  сопровождением.  Планирование НОД. Технологические карты.'
    );
?>
<div class="thumbnail thumbnail_product" style="width: 180px">
    <div class="thumbnail__image">
        <a class="thumbnail__link" href="#">
            <img class="img-responsive" src="images/thumbnail-product.jpg" alt="">
        </a>
    </div>
    <div class="thumbnail__info">
        <?php if(rand(0,1)): ?>
            <div class="thumbnail__labels">
                <?php if(rand(0,1)):?>
                    <div class="label label-red">Акция</div>
                <?php else: ?>
                    <div class="label label-orange">NEW</div>
                <?php endif;?>
                <div class="label label-<?=$LABELS[0]?>"><?=$LABELS_VALUE[$LABELS[0]]?></div>
            </div>
        <?php endif;?>
    </div>
    <div class="thumbnail__body mts">
        <div class="thumbnail__inner">
            <div class="thumbnail__actions">
                <div class="thumbnail__price mbn">
                    <div class="thumbnail__price-label thumbnail__price-label_success">
                        <i class="fum fum-check"></i>
                        есть на складе
                    </div>
                    <div class="thumbnail__price-base"><?= rand(55, 999)?> <?= rand(555, 999)?><small>,20</small> руб.</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
unset($LABELS);
unset($LABELS_VALUE);
unset($DESCRIPTIONS);
?>