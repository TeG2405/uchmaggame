<div class="thumbnail thumbnail_card">
    <?php if(rand(0,1)):?>
        <div class="thumbnail__image mbl">
            <a class="thumbnail__link" href="#">
                <img class="img-responsive" src="images/games/овощи.png" alt="">
            </a>
        </div>
        <div class="thumbnail__body">
            <a class="btn btn-primary btn-block" href="#">Овощи</a>
        </div>
    <?php else: ?>
        <div class="thumbnail__image mbl">
            <a class="thumbnail__link" href="#">
                <img class="img-responsive" src="images/games/фрукты.png" alt="">
            </a>
        </div>
        <div class="thumbnail__body">
            <a class="btn btn-primary btn-block" href="#">Фрукты</a>
        </div>
    <?php endif;?>
</div>