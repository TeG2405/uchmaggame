<?php
    $LABELS = Array('blue', 'purple', 'green');
    $LABELS_VALUE = Array(
        'blue'=> 'скидка 10%',
        'purple'=> 'скидка 15%',
        'green'=> 'скидка 5%',
    );
    shuffle($LABELS);
    $DESCRIPTIONS = Array(
        'Физическое развитие детей 2-3 лет. Планирование НОД. Технологические карты. Декабрь-февраль: 32 карты',
        'НОД. Речевое развитие детей 6-7 лет. Сентябрь - декабрь: 72 карты с методическим  сопровождением.  Планирование НОД. Технологические карты.'
    );
?>
<div class="thumbnail thumbnail_product thumbnail_transform"  itemscope itemtype="https://schema.org/Product">
    <?php if(rand(0,1)):?>
        <div class="thumbnail__image">
            <a class="thumbnail__link" href="#" itemprop="url">
                <img class="img-responsive" src="images/thumbnail-product-vertical.jpg" alt="" itemprop="image">
            </a>
            <div class="thumbnail__fast" data-toggle="modal" data-target="#MODAL_FAST_VIEW_PRODUCT"><i class="fum fum-search"></i> <span>Быстрый просмотр</span></div>
        </div>
    <?php else: ?>
        <div class="thumbnail__image">
            <a class="thumbnail__link" href="#" itemprop="url">
                <img class="img-responsive" src="images/thumbnail-product.jpg" alt="" itemprop="image">
            </a>
            <div class="thumbnail__fast" data-toggle="modal" data-target="#MODAL_FAST_VIEW_PRODUCT"><i class="fum fum-search"></i> <span>Быстрый просмотр</span></div>
        </div>
    <?php endif;?>
    <div class="thumbnail__info">
        <?php if(rand(0,1)): ?>
            <div class="thumbnail__labels">
                <?php if(rand(0,1)):?>
                    <div class="label label-red">Акция</div>
                <?php else: ?>
                    <div class="label label-orange">NEW</div>
                <?php endif;?>
                <div class="label label-<?=$LABELS[0]?>"><?=$LABELS_VALUE[$LABELS[0]]?></div>
            </div>
        <?php endif;?>
        <a href="#" class="thumbnail__reviews hidden-xs">
            <i class="fum fum-heart-glare-o"></i>
            Отзывы: <b class="text-warning">10</b>
        </a>
    </div>
    <div class="thumbnail__body">
        <div class="thumbnail__caption">
            <a class="thumbnail__title" href="#"><span itemprop="name"><?= $DESCRIPTIONS[rand(0,1)]?></span></a>
        </div>
        <div class="thumbnail__inner">
            <div class="sr-only" itemprop="description"><?= $DESCRIPTIONS[rand(0,1)]?></div>
            <div class="thumbnail__list">
                <table>
                    <tr>
                        <td>Код:</td>
                        <td itemprop="mpn">НБ-438</td>
                    </tr>
                    <tr>
                        <td>Авторы-составители:</td>
                        <td><a class="link-underline" href="#">Небыкова О.Н.</a></td>
                    </tr>
                    <tr>
                        <td>Издательство:</td>
                        <td><a class="link-underline" href="#" itemprop="brand">Учитель</a>, 2017</td>
                    </tr>
                    <tr>
                        <td>Серия:</td>
                        <td><a class="link-underline" href="#">Карточное планирование в ДОО</a></td>
                    </tr>
                    <tr>
                        <td>Страниц:</td>
                        <td>180</td>
                    </tr>
                    <tr>
                        <td>Рейтинг:</td>
                        <td>
                            <?php include "../levels/level-blocks/rating/rating.php";?>
                            <i class="sr-only" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
                                <span itemprop="ratingValue">5</span>
                                <span itemprop="reviewCount">21</span><span itemprop="bestRating">5</span><span itemprop="worstRating">0</span>
                            </i>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="thumbnail__actions">
                <div class="thumbnail__price" itemprop="offers" itemscope itemtype="https://schema.org/AggregateOffer">
                    <?php if(rand(0,1)):?>
                        <div class="thumbnail__price-label thumbnail__price-label_success">
                            <i class="fum fum-check"></i>
                            есть на складе
                        </div>
                    <?php else: ?>
                        <div class="thumbnail__price-label thumbnail__price-label_danger">
                            временно отсутствует
                        </div>
                    <?php endif;?>
                    <div class="thumbnail__price-old" itemprop="lowPrice" content="250.00"><?= rand(55, 999)?> <?= rand(555, 999)?><small>,20</small> руб.</div>
                    <div class="thumbnail__price-base" itemprop="lowPrice" content="225.00"><?= rand(1, 55)?> <?= rand(100, 500)?><small>,20</small> руб.</div>
                    <meta itemprop="priceCurrency" content="RUB " />
                </div>
                <div class="thumbnail__control">
                    <?php if(rand(0,1)):?>
                        <a class="btn btn-primary phl" data-toggle="modal" href="#MODAL_BASKET">В корзину</a>
                    <?php else: ?>
                        <a class="btn btn-default phl" data-toggle="modal" href="#MODAL_SUBSCRIBE">
                    <span class="thumbnail__btn-inner">
                        <i class="fum fum-envelope mrs"></i>
                        <small>сообщить <br> о поступлении</small>
                    </span>
                        </a>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
unset($LABELS);
unset($LABELS_VALUE);
unset($DESCRIPTIONS);
?>