<?php
$LABELS = Array('blue', 'purple', 'green');
$LABELS_VALUE = Array(
    'blue'=> 'скидка 10%',
    'purple'=> 'скидка 15%',
    'green'=> 'скидка 5%',
);
shuffle($LABELS);
$DESCRIPTIONS = Array(
    'Физическое развитие детей 2-3 лет. Планирование НОД. Технологические карты. Декабрь-февраль: 32 карты',
    'НОД. Речевое развитие детей 6-7 лет. Сентябрь - декабрь: 72 карты с методическим  сопровождением.  Планирование НОД. Технологические карты.'
);
?>
    <div class="thumbnail thumbnail_product thumbnail_transform">
        <?php if(rand(0,1)):?>
            <div class="thumbnail__image">
                <a class="thumbnail__link" href="#">
                    <img class="img-responsive" src="images/thumbnail-product-vertical.jpg" alt="">
                </a>
                <div class="thumbnail__fast" data-toggle="modal" data-target="#MODAL_FAST_VIEW_PRODUCT"><i class="fum fum-search"></i> <span>Быстрый просмотр</span></div>
            </div>
        <?php else: ?>
            <div class="thumbnail__image">
                <a class="thumbnail__link" href="#">
                    <img class="img-responsive" src="images/thumbnail-product.jpg" alt="">
                </a>
                <div class="thumbnail__fast" data-toggle="modal" data-target="#MODAL_FAST_VIEW_PRODUCT"><i class="fum fum-search"></i> <span>Быстрый просмотр</span></div>
            </div>
        <?php endif;?>
        <div class="thumbnail__info">
            <?php if(rand(0,1)): ?>
                <div class="thumbnail__labels">
                    <?php if(rand(0,1)):?>
                        <div class="label label-red">Акция</div>
                    <?php else: ?>
                        <div class="label label-orange">NEW</div>
                    <?php endif;?>
                    <div class="label label-<?=$LABELS[0]?>"><?=$LABELS_VALUE[$LABELS[0]]?></div>
                </div>
            <?php endif;?>
            <a href="#" class="thumbnail__reviews hidden-xs">
                <i class="fum fum-heart-glare-o"></i>
                Отзывы: <b class="text-warning">10</b>
            </a>
        </div>
        <div class="thumbnail__body">
            <div class="thumbnail__caption">
                <a class="thumbnail__title" href="#"><?= $DESCRIPTIONS[rand(0,1)]?></a>
            </div>
            <div class="thumbnail__inner">
                <div class="thumbnail__list">
                    <table>
                        <tr>
                            <td>Код:</td>
                            <td>НБ-438</td>
                        </tr>
                        <tr>
                            <td>Авторы-составители:</td>
                            <td><a class="link-underline" href="#">Небыкова О.Н.</a></td>
                        </tr>
                        <tr>
                            <td>Издательство:</td>
                            <td><a class="link-underline" href="#">Учитель</a>, 2017</td>
                        </tr>
                        <tr>
                            <td>Серия:</td>
                            <td><a class="link-underline" href="#">Карточное планирование в ДОО</a></td>
                        </tr>
                        <tr>
                            <td>Страниц:</td>
                            <td>180</td>
                        </tr>
                        <tr>
                            <td>Рейтинг:</td>
                            <td><?php include "../levels/level-blocks/rating/rating.php";?></td>
                        </tr>
                    </table>
                </div>
                <div class="thumbnail__actions">
                    <div class="thumbnail__price">
                        <?php if(rand(0,1)):?>
                            <div class="thumbnail__price-label thumbnail__price-label_success">
                                <i class="fum fum-check"></i>
                                есть на складе
                            </div>
                        <?php else: ?>
                            <div class="thumbnail__price-label thumbnail__price-label_danger">
                                временно отсутствует
                            </div>
                        <?php endif;?>
                        <div class="thumbnail__price-old"><?= rand(55, 999)?> <?= rand(555, 999)?><small>,20</small> руб.</div>
                        <div class="thumbnail__price-base"><?= rand(1, 55)?> <?= rand(100, 500)?><small>,20</small> руб.</div>
                    </div>
                    <div class="thumbnail__control">
                        <a class="btn btn-primary phl" data-toggle="modal" href="#MODAL_BASKET">В подарок</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
unset($LABELS);
unset($LABELS_VALUE);
unset($DESCRIPTIONS);
?>