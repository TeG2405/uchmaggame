<nav>
    <ul class="pagination mvn" data-toggle="pagination">
        <li class="pagination__li"><a class="pagination__link" href="#">1</a></li>
        <li class="pagination__li prev phn swiper-button-disabled"><span class="pagination__link pagination__link_dotted">...</span></li>
        <li class="swiper-container pagination__container">
            <ul class="swiper-wrapper pagination mvn">
                <li class="swiper-slide pagination__li pagination__li_active pagination__li_double">
                    <span class="pagination__link pagination__link_active pagination__link_double">2 - 3</span>
                </li>
                <?php for($i = 4; $i < 30; $i++):?>
                    <li class="swiper-slide pagination__li"><a class="pagination__link" href="#"><?=$i?></a></li>
                <?php endfor;?>
            </ul>
        </li> 
        <li class="pagination__li next phn swiper-button-disabled"><span class="pagination__link pagination__link_dotted">...</span></li>
        <li class="pagination__li"><a class="pagination__link" href="#">30</a></li>
    </ul>
</nav> 