<nav>
    <ul class="pagination mvn">
        <li class="pagination__li pagination__li_active"><a class="pagination__link pagination__link_active" href="#">1</a></li>
        <?php for($i = 2; $i < 5; $i++):?>
            <li class="pagination__li"><a class="pagination__link" href="#"><?=$i?></a></li>
        <?php endfor;?>
        <li class="pagination__li">
            <span class="pagination__link pagination__link_dotted">...</span>
            <a class="pagination__link" href="#">15</a>
        </li>
        <li class="pagination__li"><a class="pagination__link" href="#">30</a></li>
    </ul>
</nav> 