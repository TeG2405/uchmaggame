/**
 * Created by zvezdilin on 11.01.2018.
 */
+function($){
    var Block = function(element, option){
        this.$block = null;
        this.$inner = null;
        this.$next = null;
        this.$prev = null;
        this.SwiperApi = null;
        this.init(element, option);
    };

    Block.prototype.init = function(element, option){
        this.$block = $(element);
        this.$inner = this.$block.find('.pagination__container');
        this.$next = this.$block.find('.next');
        this.$prev = this.$block.find('.prev');
        try {
            this.SwiperApi = new Swiper(this.$inner, {
                slidesPerView: 'auto',
                slidesPerGroup: 2,
                spaceBetween: 0,
                freeMode: false,
                freeModeMomentum: false,
                nextButton: this.$next[0],
                prevButton: this.$prev[0]
            });
        }
        catch (e){
            console.log(e + ' - Swiper.js not supported');
        }
    };


    function Plugin(option){
        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('block.pagination');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('block.pagination', (data = new Block(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    $.fn.block_pagination = Plugin;
    $.fn.block_pagination.Constructor = Block;

    $('.pagination[data-toggle="pagination"]').block_pagination();
}(jQuery);