<div class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
    <div class="swiper-container breadcrumbs__container">
        <ol class="swiper-wrapper breadcrumbs__wrapper">
            <li class="swiper-slide breadcrumbs__li" itemscope itemprop="itemListElement" itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="1" />
                <a class="breadcrumbs__link" href="#" itemprop="item">
                    <span itemprop="name">Главная</span>
                </a>
            </li>
            <li class="swiper-slide breadcrumbs__li" itemscope itemprop="itemListElement" itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="2" />
                <a class="breadcrumbs__link" href="#" itemprop="item"><span itemprop="name">Каталог</span></a>
                <div class="breadcrumbs__dropdown">
                    <ul class="dropdown-menu">              
                        <li><a href="#">ЕГЭ, ОГЭ, ВПР</a></li>
                        <li><a href="#">Зимняя распродажа</a></li>
                        <li><a href="#">Подарки к праздникам</a></li>
                        <li><a href="#">Преподавателям</a></li>
                        <li><a href="#">Администрации</a></li>
                        <li><a href="#">Психологам и логопедам</a></li>
                        <li><a href="#">Педагогам ДОО</a></li>
                        <li><a href="#">Учащимся и абитуриентам</a></li>
                        <li><a href="#">Студентам</a></li>
                        <li><a href="#">Родителям</a></li>
                        <li><a href="#">Учебники</a></li>
                        <li><a href="#">Пособия на CD/DVD</a></li>
                        <li><a href="#">Версии для скачивания</a></li>
                        <li><a href="#">Детский мир</a></li>
                        <li><a href="#">Мир занятий, увлечений</a></li>
                        <li><a href="#">Подарочная продукция</a></li>
                        <li><a href="#">Канцелярские товары. Школьные принадлежности</a></li>
                        <li><a href="#">Журнально-бланочная продукция</a></li>
                        <li><a href="#">Грамоты, дипломы</a></li>
                        <li><a href="#">Наглядно-дидактический материал</a></li>
                        <li><a href="#">Переподготовка. Курсы. Вебинары</a></li>
                        <li><a href="#">Электронные продукты</a></li>
                        <li><a href="#">Подарочные сертификаты</a></li>
                    </ul>
                </div>
            </li>
            <li class="swiper-slide breadcrumbs__li" itemscope itemprop="itemListElement" itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="3" />
                <a class="breadcrumbs__link" href="#" itemprop="item">
                    <span itemprop="name">УчМаг</span>
                </a>
            </li>
            <li class="swiper-slide breadcrumbs__li" itemscope itemprop="itemListElement" itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="4" />
                <a class="breadcrumbs__link" href="#" itemprop="item">
                    <span itemprop="name">Администрации</span>
                </a>
            </li>
            <li class="swiper-slide breadcrumbs__li" itemscope itemprop="itemListElement" itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="5" />
                <span class="breadcrumbs__link" itemprop="item">
                    Физическая культура. 1-11 классы: комплексная программа физического воспитания учащихся В. И. Ляха, А. А. Зданевича
                </span>
            </li>
        </ol>
    </div>
</div>