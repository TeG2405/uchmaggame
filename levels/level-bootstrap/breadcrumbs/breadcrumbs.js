/**
 * Created by zvezdilin on 10.01.2018.
 */
+function($){
    var Block = function(element, option){
        this.$block = null;
        this.$inner = null;
        this.SwiperApi = null;

        this.init(element, option);
    };

    Block.prototype.init = function(element, option){
        this.$block = $(element);
        this.$inner = this.$block.find('.breadcrumbs__container');
        try {
            this.SwiperApi = new Swiper(this.$inner, {
                slidesPerView: 'auto',
                spaceBetween: 0,
                freeMode: true,
                freeModeMomentum: false
            });
        }
        catch (e){
            console.log(e + ' - Swiper.js not supported');
        }
    };
  

    function Plugin(option){
        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('block.breadcrumbs');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('block.breadcrumbs', (data = new Block(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    $.fn.block_breadcrumbs = Plugin;
    $.fn.block_breadcrumbs.Constructor = Block;

    $('.breadcrumbs').block_breadcrumbs();
}(jQuery);