<div class="list-group">
    <a class="list-group-item phg active collapsed" data-toggle="collapse" href="#CATALOG_DETAIL_MODE_ACTIONS" role="button">
        <div class="list-group-counter">
            <div class="mvs mrl">
                <span class="link-dashed">До окончания акции осталось</span>
                <i class="fum fum-caret-down"></i>
            </div>
            <div class="mvs">
                <?php include "../levels/level-blocks/time-counter/time-counter.php";?>
            </div>
        </div>
    </a>
    <div class="collapse" id="CATALOG_DETAIL_MODE_ACTIONS">
        <?php for($i = 0; $i < 3; $i++):?>
            <div class="list-group-item phg">
                <?php if(false):?>
                    <span class="badge phm">До 23:59 25 февраля</span>
                <?php endif;?>
                Скидка 25% по акции <a href="#">Помогаем коррекционным педагогам</a>
            </div>
        <?php endfor;?>
    </div>
</div>