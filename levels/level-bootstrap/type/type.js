/**
 * Created by zvezdilin on 28.12.2017.
 */
+function($){
    var Block = function(element, option){
        this.$block = null;
        this.init(element, option);
    };

    Block.prototype.init = function(element, option){
        this.$block = $(element);
        if(this.$block.data('type') == 'year'){
            this.$block.datepicker({
                minViewMode: 2,
                format: 'yyyy',
                language: "ru",
                templates: {
                    leftArrow: '<span class="fum fum-angle-left"></span>',
                    rightArrow: '<span class="fum fum-angle-right"></span>'
                }
            }).attr('readonly', true);
        }
        if(this.$block.data('type') == 'month'){
            this.$block.datepicker({
                minViewMode: 1,
                format: 'MM yyyy',
                language: "ru",
                templates: {
                    leftArrow: '<span class="fum fum-angle-left"></span>',
                    rightArrow: '<span class="fum fum-angle-right"></span>'
                }
            }).attr('readonly', true);
        }
    };

    function Plugin(option){
        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('block.type');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('block.type', (data = new Block(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    $.fn.block_type = Plugin;
    $.fn.block_type.Constructor = Block;

    $('[data-type]').block_type();
}(jQuery);