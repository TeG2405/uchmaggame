<div class="well well-container mtl mbg">
    <ul class="list-circle list-column text-primary text-small mbn">
        <li><a href="#">Родителям и детям. Растём самостоятельными</a></li>
        <li><a href="#">Занятия с детьми дошкольного возраста</a></li>
        <li><a href="#">Занятия с учащимися начальной школы</a></li>
        <li><a href="#">Занятия с учащимися средней школы</a></li>
        <li><a href="#">Воспитательная литература</a></li>
        <li><a href="#">Портфолио</a></li>
        <li><a href="#">Детские книги</a></li>
        <li><a href="#">Табели, свидетельства</a></li>
        <li><a href="#">Паспарту</a></li>
        <li><a href="#">Товары к школе</a></li>
    </ul>
</div> 