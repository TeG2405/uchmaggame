<div class="swiper-container carousel-row carousel-row_long">
    <div class="swiper-wrapper carousel-row__wrapper">
        <?php for($i=0; $i < 8; $i++):?>
        <div class="swiper-slide carousel-row__slide pbl">
            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card.php";?>
        </div>
        <?php endfor;?>
    </div>
    <div class="carousel-row__arrow carousel-row__arrow_prev" style="top: 120px">
        <i class="fum fum-angle-left"></i>
    </div>
    <div class="carousel-row__arrow carousel-row__arrow_next" style="top: 120px">
        <i class="fum fum-angle-right"></i>
    </div>
</div>