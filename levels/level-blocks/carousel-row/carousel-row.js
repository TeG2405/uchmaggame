/**
 * Created by zvezdilin on 22.11.2017.
 */
+function($){
    var Block = function(element, option){
        this.$block = null;
        this.SwiperApi = null;
        this.init(element, option);
    };

    Block.prototype.init = function(element, option){
        this.$block = $(element);
        try {
            this.SwiperApi =  new Swiper(this.$block, {
                slidesPerView: 'auto',
                spaceBetween: 0,
                freeMode: false,
                nextButton: this.$block.find('.carousel-row__arrow_next'),
                prevButton: this.$block.find('.carousel-row__arrow_prev')
            })
        }
        catch (e){
            console.log(e + ' - Swiper.js not supported');
        }
    };

    function Plugin(option){
        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('block.carouselRow');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('block.carouselRow', (data = new Block(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    $.fn.block_carouselRow = Plugin;
    $.fn.block_carouselRow.Constructor = Block;

    $('.carousel-row').filter(':visible').block_carouselRow();
    $('.modal').on('shown.bs.modal', function (e) {
        $('.carousel-row').filter(':visible').block_carouselRow();
    });
}(jQuery);

