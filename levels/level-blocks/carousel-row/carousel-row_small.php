<div class="swiper-container carousel-row carousel-row_small">
    <div class="swiper-wrapper carousel-row__wrapper">
        <?php for($i=0; $i < 8; $i++):?>
        <div class="swiper-slide carousel-row__slide">
            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_product_small.php";?>
        </div>
        <?php endfor;?>
    </div>
    <div class="carousel-row__arrow carousel-row__arrow_prev" style="top: 150px">
        <i class="fum fum-angle-left"></i>
    </div>
    <div class="carousel-row__arrow carousel-row__arrow_next" style="top: 150px">
        <i class="fum fum-angle-right"></i>
    </div>
</div>