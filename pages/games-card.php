<?php
define("PAGE_ID", "GAMES_CARD");
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<?php include "../levels/level-blocks/head/head.php";?>
<body>
<?php include "../levels/level-blocks/header/header.php";?>
<main class="main">
    <div class="container">
        <div class="main__row">
            <div class="main__inner">
                <div class="main__flow_clean">
                <div class="main__flow_restore">
                    <div class="main__title">
                        <h1 class="mvn mrl">Овощи</h1>
                    </div>
                    <div class="clearfix">
                        <img class="img-responsive pull-right" align="right" src="images/games/овощи.png" alt="">
                        <p class="lead">Представленные ниже тесты отображают тему "Овощи", задания расположены от простого к сложному, и включают в себя не только обобщения понятия "Овощи", и их название, но и также группировку по цвету, форме, по определенным признакам и месту произростания.</p>
                        <a class="btn btn-warning phl" href="#">Обучение</a>
                    </div>
                    <h2>Выбирите задание:</h2>
                    <div class="list-group list-group_space">
                        <button type="button" class="btn mbs list-group-item list-group-item-success"><i class="fum fum-check mrs"></i> Задание 1 <i style="font: initial">→</i> привет из сада-огорода</button>
                        <button type="button" class="btn mbs list-group-item list-group-item-primary">Задание 2 <i style="font: initial">→</i> с грядки овощи собрали</button>
                        <button type="button" class="btn mbs list-group-item list-group-item-primary">Задание 3 <i style="font: initial">→</i> Morbi leo risus</button>
                        <button type="button" class="btn mbs list-group-item list-group-item-primary">Задание 4 <i style="font: initial">→</i> Porta ac consectetur ac</button>
                    </div>
                    
                    <div class="well well-container mvg">
                        <p>Воспользуйтесь <span class="text-warning">бесплатной</span> программой для Android для самостоятельного обучения ребенка</p>
                        <a class="btn btn-primary-light" href="#">Программа</a>
                    </div>

                    <h2>Карточки по теме</h2>
                    <?php include "../levels/level-blocks/carousel-row/carousel-row_card_long.php";?>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/swiper/dist/js/swiper.js"></script>
<script src="../levels/level-blocks/carousel-row/carousel-row.js"></script>
</body>
</html>