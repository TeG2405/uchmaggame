<?php
define("PAGE_ID", "GAMES_ANSWER");
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<?php include "../levels/level-blocks/head/head.php";?>
<body>
<?php include "../levels/level-blocks/header/header.php";?>
<main class="main">
    <div class="container">
        <div class="main__row">
            <div class="main__inner">
                <div class="main__flow_clean">
                <div class="main__flow_restore">
                    <div class="main__title">
                        <h1 class="mvn mrl">Огурец найди и скорее покажи!</h1>
                    </div>
                    <div class="card-list mvn pvn" id="AJAX">
                        <div class="card-list__item col-xs-6">
                            <div class="thumbnail thumbnail_card" onclick="(this.className += ' thumbnail_success') && YEAP.play()">
                                <div class="thumbnail__image">
                                    <span class="thumbnail__link" href="#">
                                        <img class="img-responsive" src="images/games/4.png" alt="">
                                    </span>
                                    <span class="thumbnail__title">Правильно</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-list__item col-xs-6">
                            <div class="thumbnail thumbnail_card" onclick="(this.className += ' thumbnail_danger') && NOT.play()">
                                <div class="thumbnail__image">
                                    <span class="thumbnail__link" href="#">
                                        <span class="loader"></span>
                                        <img class="img-responsive" src="images/games/3.png" alt="">
                                    </span>
                                    <span class="thumbnail__title">Попробуй еще раз</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-list__item col-xs-6">
                            <div class="thumbnail thumbnail_card" onclick="(this.className += ' thumbnail_danger') && NOT.play()">
                                <div class="thumbnail__image">
                                    <span class="thumbnail__link" href="#">
                                        <img class="img-responsive" src="images/games/2.png" alt="">
                                    </span>
                                    <span class="thumbnail__title">Попробуй еще раз</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-list__item col-xs-6">
                            <div class="thumbnail thumbnail_card" onclick="(this.className += ' thumbnail_danger') && NOT.play()">
                                <div class="thumbnail__image">
                                    <span class="thumbnail__link" href="#">
                                        <img class="img-responsive" src="images/games/1.png" alt="">
                                    </span>
                                    <span class="thumbnail__title">Попробуй еще раз</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <audio class="sr-only" id="YEAP" onended="(function() {
                        AJAX.className += ' fade';
                         setTimeout(function() {
                        AJAX.className += ' in';
                        }, 500);

                    })()" src="upload/Молодец.ogg"></audio>
                    <audio class="sr-only" id="NOT" src="upload/Выбери%20еще%20раз.ogg"></audio>
                    <audio class="sr-only" src="upload/Молодец.ogg" autoplay></audio>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>