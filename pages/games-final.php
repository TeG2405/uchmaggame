<?php
define("PAGE_ID", "GAMES_FINAL");
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<?php include "../levels/level-blocks/head/head.php";?>
<body>
<?php include "../levels/level-blocks/header/header.php";?>
<main class="main">
    <div class="container">
        <div class="main__row">
            <div class="main__inner">
                <div class="main__flow_clean">
                <div class="main__flow_restore">
                    <div class="main__title">
                        <h1 class="mvn mrl">Молодец!</h1>
                    </div>
                    <div class="row">
                        <div class="col-xs-12  col-sm-6 col-sm-push-6">
                            <div class="mbl">
                                <img style="margin: auto" class="img-responsive" src="images/games/win.jpg" alt="">
                            </div>
                            <a class="btn btn-primary btn-block" style="max-width: 320px; margin: auto" href="#">Следующее задание <i>→</i></a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-sm-pull-6">
                            <div class="mbl">
                                <img style="margin: auto" class="img-responsive hidden-xs" src="images/games/final.jpg" alt="">
                            </div>
                            <div class="social-likes social-likes_horizontal pvx" data-url="http://landscapists.info/">
                                <div class="social-likes__title hidden-xs">Поделиться</div>
                                <div class="facebook" title="Поделиться ссылкой на Фейсбуке"></div>
                                <div class="twitter" title="Поделиться ссылкой в Твиттере"></div>
                                <div class="vkontakte" title="Поделиться ссылкой во Вконтакте"></div>
                                <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках"></div>
                                <div class="plusone" title="Поделиться ссылкой в Гугл-плюсе"></div>
                            </div>
                        </div>
                    </div>
                    <audio class="sr-only" onended="alert('Аудио воспроизведено пора делать переход')" src="upload/У%20тебя%20получилось.ogg" autoplay></audio>
                    
                    <div class="well well-container mvg">
                        <p>Установите бесплантую программу на Android для самостоятельного обучения ребенка</p>
                        <a class="btn btn-primary-light" href="#">Программа</a>
                    </div>

                    <h2>Карточки по теме</h2>
                    <?php include "../levels/level-blocks/carousel-row/carousel-row_card_long.php";?>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/swiper/dist/js/swiper.js"></script>
<script src="../levels/level-blocks/carousel-row/carousel-row.js"></script>
<script src="../bower_components/social-likes/dist/social-likes.min.js"></script>
</body>
</html>