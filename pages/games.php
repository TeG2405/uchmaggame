<?php
define("PAGE_ID", "GAMES");
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<?php include "../levels/level-blocks/head/head.php";?>
<body>
<?php include "../levels/level-blocks/header/header.php";?>
<main class="main">
    <div class="container">
        <div class="main__row">
            <div class="main__inner">
                <div class="main__flow_clean">
                <div class="main__flow_restore">
                    <div class="main__title">
                        <h1 class="mvn mrl">Развивающие карточки</h1>
                    </div>
                    <p>Специально для заботливых родителей и любознательных малышей мы предлагаем систему тестовых заданий, которые помогут вам разобраться в том, что уже знает ваш малыш, чего еще не знает, и как научить его тому, что он должен знать.</p>
                    <p><a href="#">Посмотреть советы по выполнению тестов</a> и выбрать темы для проверки знаний.</p>

                    <div class="card-list mvn pvn">
                        <div class="card-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card_transform.php";?>
                        </div>
                        <div class="card-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card_transform.php";?>
                        </div>
                        <div class="card-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card_transform.php";?>
                        </div>
                        <div class="card-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card_transform.php";?>
                        </div>
                        <div class="card-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card_transform.php";?>
                        </div>
                        <div class="card-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card_transform.php";?>
                        </div>
                        <div class="card-list__item">
                            <?php include "../levels/level-bootstrap/thumbnail/thumbnail_card_transform.php";?>
                        </div>
                    </div>
                    
                    <div class="well well-container mvg">
                        <p>Установите бесплантую программу на Android для самостоятельного обучения ребенка</p>
                        <a class="btn btn-primary-light" href="#">Программа</a>
                    </div>

                    <h2>Карточки по теме</h2>
                    <?php include "../levels/level-blocks/carousel-row/carousel-row_card_long.php";?>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/swiper/dist/js/swiper.js"></script>
<script src="../levels/level-blocks/carousel-row/carousel-row.js"></script>
</body>
</html>