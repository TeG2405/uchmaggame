<?php
define("PAGE_ID", "GAMES_AUTHORIZATION");
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<?php include "../levels/level-blocks/head/head.php";?>
<body>
<?php include "../levels/level-blocks/header/header.php";?>
<main class="main">
    <div class="container">
        <div class="main__row">
            <div class="main__inner">
                <div class="main__flow_clean">
                <div class="main__flow_restore">
                    <div class="main__title">
                        <h1 class="mvn mrl">Авторизация</h1>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="LOGIN_EMAIL">Логин или E-mail:</label>
                                    <input type="email" class="form-control" id="LOGIN_EMAIL" placeholder="Логин / E-mail">
                                </div>
                                <div class="form-group">
                                    <label for="LOGIN_PASSWORD">Пароль:</label>
                                    <input type="password" class="form-control" id="LOGIN_PASSWORD" placeholder="Пароль">
                                </div>
                                <div class="form-group">
                                    <?php include "../levels/level-blocks/social/social-circle.php";?>
                                </div>
                                <div class="form-group modal-controls mbs">
                                    <div class="modal-controls__col">
                                        <a class="btn btn-primary btn-lg" href="?login=Y">Вход</a>
                                    </div>
                                    <div class="modal-controls__col  text-right">
                                        <button class="btn btn-default btn-lg" type="button"  data-dismiss="modal" aria-label="Close">Регистрация</button>
                                    </div>
                                </div>
                                <a class="link-underline" href="#">Забыли свой пароль?</a>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>