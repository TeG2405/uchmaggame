<?php
define("PAGE_ID", "GAMES_QUESTION");
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<?php include "../levels/level-blocks/head/head.php";?>
<body>
<?php include "../levels/level-blocks/header/header.php";?>
<main class="main">
    <div class="container">
        <div class="main__row">
            <div class="main__inner">
                <div class="main__flow_clean">
                <div class="main__flow_restore">
                    <div class="main__title">
                        <h1 class="mvn mrl">Овощи</h1>
                    </div>
                    <div class="game-row">
                        <div class="game-row__image">
                            <img src="images/games/овощи.png" alt="">
                        </div>
                        <div class="game-row__text">
                            <p class="lead">В огороде растут, <br> и на грядках спеют, <br> Овощь нужный отъискать <br> Мы с тобой сумеем!</p>
                            <a class="btn btn-primary" href="#">Продолжить</a>
                        </div>
                    </div>
                    <audio class="sr-only" onended="alert('Аудио воспроизведено пора делать переход')" src="upload/Молодец.ogg" autoplay></audio>
                </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>