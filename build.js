const fs = require('fs');
const path = require('path');
const postcss = require('postcss');


const CSS = {
    "bootstrap": [
        path.resolve(__dirname, 'pages/styles/bootstrap.css'),
        path.resolve(__dirname, 'pages/styles/bootstrap.min.css')
    ],
    "template_styles_autogenerated": [
        path.resolve(__dirname, 'pages/styles/template_styles_autogenerated.css'),
        path.resolve(__dirname, 'pages/styles/template_styles_autogenerated.min.css')
    ],
};

Object.keys(CSS).map(function (template) {
    let src = CSS[template];
    fs.readFile(src[0], (err, css) => {
        postcss([
            require('postcss-easysprites')({
                imagePath: path.resolve(__dirname, 'pages/images/'),
                spritePath: path.resolve(__dirname, 'pages/images/_sprite/', template)
            }),
            require('autoprefixer')({
                browsers: [
                    "Explorer >= 10",
                    "Firefox >= 4",
                    "Opera >= 12",
                    "Chrome >= 15",
                    "Safari >= 5",
                    "Android 2.3",
                    "Android >= 4",
                    "iOS >= 6"
                ]
            }),
            require('postcss-cachebuster')({
                imagesPath : '/pages/images/',
                cssPath : '/pages/styles/',
            }),
            require('postcss-import')(),
            require('cssnano')(),
        ])
        .process(css, { from: src[0], to: src[1] })
        .then(result => {
            fs.writeFile( path.resolve(__dirname, src[1]), result.css);
        });
    });
});
